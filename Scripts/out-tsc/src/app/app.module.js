import * as tslib_1 from "tslib";
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { APP_BASE_HREF } from '@angular/common';
import { DashboardsComponent } from './dashboards/dashboards.component';
import { DxChartModule, DxPieChartModule } from 'devextreme-angular';
import { Ng2FlatpickrModule } from 'ng2-flatpickr';
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
let AppModule = class AppModule {
};
AppModule = tslib_1.__decorate([
    NgModule({
        declarations: [
            AppComponent,
            DashboardsComponent
        ],
        imports: [
            BrowserModule,
            AppRoutingModule,
            DxChartModule,
            Ng2FlatpickrModule,
            AngularMultiSelectModule,
            FormsModule,
            ReactiveFormsModule,
            DxPieChartModule
        ],
        providers: [
            {
                provide: APP_BASE_HREF,
                useValue: '/QMS/DashBoard/QMS_Dashboard'
            }
        ],
        bootstrap: [AppComponent]
    })
], AppModule);
export { AppModule };
//# sourceMappingURL=app.module.js.map