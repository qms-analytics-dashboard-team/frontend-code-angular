import { Component, OnInit } from '@angular/core';
import { CCNDataStructure, DashboardsService, OverdueDataStructure, QualityEventsDataStructure } from '../services/dashboards.service';
import { FlatpickrOptions } from 'ng2-flatpickr';
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-dashboards',
  templateUrl: './dashboards.component.html',
  styleUrls: ['./dashboards.component.scss']
})
export class DashboardsComponent implements OnInit {

  filtersForm:FormGroup;

  dataSource: QualityEventsDataStructure[];

  departmentsList = [];
  selectedDepartments = [];
  departmentDropdownSettings = {};

  eventsDropdownSettings={};
  qualityEventsList=[];
  selectedQualityEvents=[];

  changeCategoriesList=[];
  selectedChangeCategory=[];
  changeCategoryDropdownSettings={};

  changeClassificationList=[];
  selectedChangeClassification=[];
  changeClassificationDropdownSettings={};


  isFirstLevel:boolean;
  level:number;

  exampleOptions: FlatpickrOptions = {
  // defaultDate: '2021-05-15',
  minDate:'2021-05-15'
};

  CCNData : CCNDataStructure[];

  overdueData:OverdueDataStructure[];


  constructor(private dashboardService:DashboardsService, private fb:FormBuilder) { 
    this.level=0;
    
    this.filtersForm=this.fb.group({
      fromDate:[''],
      toDate:['']
    })
  }
  
  ngOnInit(){
    // this.selectedDepartments=this.departmentsList;
    //   this.selectedDepartments = [
    //     {"id":1,"itemName":"QA"},
    //     {"id":2,"itemName":"HR"},
    //     {"id":3,"itemName":"Ware house"}
    // ];
      this.departmentDropdownSettings = { 
        singleSelection: false, 
        text:"Select Departments",
        selectAllText:'Select All',
        unSelectAllText:'UnSelect All',
        enableSearchFilter: true,
        classes:"myclass custom-class"
      };

      this.dataSource = this.dashboardService.getQualityEventsData('');
    for(let i=0;i<this.dataSource.length;i++){
    this.departmentsList.push({"id":i+1,"itemName":this.dataSource[i].department});
    this.selectedDepartments.push({"id":i+1,"itemName":this.dataSource[i].department});
    }

      this.eventsDropdownSettings = { 
                                singleSelection: false, 
                                text:"Select Events",
                                selectAllText:'Select All',
                                unSelectAllText:'UnSelect All',
                                enableSearchFilter: true,
                                classes:"myclass custom-class"
                              };      
      this.qualityEventsList = [
                                {"id":1,"itemName":"CCN"},
                                {"id":2,"itemName":"Incident"},
                                {"id":3,"itemName":"Errata"},
                                {"id":4,"itemName":"Note to File"}
                              ];
      this.selectedQualityEvents = [
                                {"id":1,"itemName":"CCN"},
                                {"id":2,"itemName":"Incident"},
                                {"id":3,"itemName":"Errata"},
                                {"id":4,"itemName":"Note to File"}
                              ];
      this.changeCategoriesList=[
        {"id":1,"itemName":'Permanent'},
        {"id":2,"itemName":'Temporary'}
      ]
      this.selectedChangeCategory=[];

      this.changeCategoryDropdownSettings={
        singleSelection: false, 
                                text:"Select Change Category",
                                selectAllText:'Select All',
                                unSelectAllText:'UnSelect All',
                                enableSearchFilter: true,
                                classes:"myclass custom-class"
      }

      this.changeClassificationList=[
        {"id":1,"itemName":'Major'},
        {"id":2,"itemName":'Minor'}
      ]
      this.selectedChangeClassification=[];
      
      this.changeClassificationDropdownSettings={
        singleSelection: false, 
                                text:"Select Change Classification",
                                selectAllText:'Select All',
                                unSelectAllText:'UnSelect All',
                                enableSearchFilter: true,
                                classes:"myclass custom-class"
      }
  }
  onItemSelect(item:any){
      this.dataSource=[];
      for(let i=0;i<this.selectedDepartments.length;i++){
        let deptData= this.dashboardService.getQualityEventsData(this.selectedDepartments[i].itemName);
        this.dataSource.push(deptData[0]);
      }
  }
  onItemDeSelect(item:any){
    console.log(item);
    console.log(this.selectedDepartments);
    console.log(this.departmentsList);
    this.dataSource=[];
    if(this.selectedDepartments.length===0){
      this.selectedDepartments.push(item);
      let deptData= this.dashboardService.getQualityEventsData(this.selectedDepartments[0].itemName);
        this.dataSource.push(deptData[0]);
    }
    // else if(this.selectedDepartments.length===1){
    //   let deptData= this.dashboardService.getQualityEventsData(this.selectedDepartments[0].itemName);
    //     this.dataSource.push(deptData[0]);
    //   // this.dataSource.push(item);
    // }
    else{
      for(let i=0;i<this.selectedDepartments.length;i++){
        let deptData= this.dashboardService.getQualityEventsData(this.selectedDepartments[i].itemName);
        this.dataSource.push(deptData[0]);
      }
    }
    
  }

  onSelectAll(items: any){
    this.dataSource=[];
    for(let i=0;i<items.length;i++){
      let deptData= this.dashboardService.getQualityEventsData(items[i].itemName);
      this.dataSource.push(deptData[0]);
    }
      console.log(items);
  }
  onDeSelectAll(items: any){
    console.log("deselect all",items);
    this.dataSource=[];
    for(let i=0;i<items.length;i++){
      let deptData= this.dashboardService.getQualityEventsData(items[i].itemName);
      this.dataSource.push(deptData[0]);
    }
      console.log(items);
  }

  onEventSelect(items:any){

// console.log(`Original array elements: ${array}`);
// console.log(`Elements to delete: ${toDelete}`);

// let newArray = array.filter(n => !toDelete.includes(n));
// console.log(`Array elements after splice: ${newArray}`);

console.log("selected events",this.selectedQualityEvents);
let excludedEvents=[];
for(let i=0;i<this.qualityEventsList.length;i++){
  let count=0;
  for(let j=0;j<this.selectedQualityEvents.length;j++){
    if(this.selectedQualityEvents[j].id===this.qualityEventsList[i].id){
      count++;
  }
  
  }
  if(count===0){
    excludedEvents.push(this.qualityEventsList[i]);
  }
}
console.log("exclu",excludedEvents);

    for(let i=0;i<this.selectedDepartments.length;i++){
      let deptData= this.dashboardService.getQualityEventsDataOfSelectedDeptAndEvent(excludedEvents,this.selectedDepartments[i].itemName);
      // this.dataSource.push(deptData[0]);
    }

  }

  onEventDeSelect(items:any){
    console.log("selected events",this.selectedQualityEvents);
    let excludedEvents=[];
for(let i=0;i<this.qualityEventsList.length;i++){
  let count=0;
  for(let j=0;j<this.selectedQualityEvents.length;j++){
    if(this.selectedQualityEvents[j].id===this.qualityEventsList[i].id){
      count++;
  }
  
  }
  if(count===0){
    excludedEvents.push(this.qualityEventsList[i]);
  }
}
console.log("exclu",excludedEvents);

  }

  onSelectAllEvents(items:any){
    console.log("selected events",this.selectedQualityEvents);
    let excludedEvents=[];
    for(let i=0;i<this.qualityEventsList.length;i++){
      let count=0;
      for(let j=0;j<this.selectedQualityEvents.length;j++){
        if(this.selectedQualityEvents[j].id===this.qualityEventsList[i].id){
          count++;
      }
      }
      if(count===0){
        excludedEvents.push(this.qualityEventsList[i]);
      }
    }
console.log("exclu",excludedEvents);
  }

  onDeSelectAllEvents(items:any){
    console.log("selected events",this.selectedQualityEvents);
    let excludedEvents=[];
    for(let i=0;i<this.qualityEventsList.length;i++){
      let count=0;
      for(let j=0;j<this.selectedQualityEvents.length;j++){
        
        if(this.selectedQualityEvents[j].id===this.qualityEventsList[i].id){
          count++;
      }
      
      }
      if(count===0){
        excludedEvents.push(this.qualityEventsList[i]);
      }
    }
console.log("exclu",excludedEvents);
  }

  onChangeCategorySelect(event){

  }

  onChangeCategoryDeSelect(event){}
  onSelectAllChangeCategories(event){}

  onDeSelectAllChangeCategories(event){}

  onChangeClassificationSelect(event){}

  onChangeClassificationDeSelect(event){}

  onSelectAllChangeClassifications(event){}

  onDeSelectAllChangeClassifications(event){}

  onPointClick(event){
    console.log(event);
    console.log(event.target.originalValue + ' ' +event.target.series.name + 's of ' + event.target.originalArgument );
    this.level++;

    if(event.element.id==='chart'){
      if(event.target.series.name === 'CCN' && event.target.originalArgument === 'QA'){
        this.selectedDepartments = [{"id":1,"itemName":"QA"}];
        this.isFirstLevel=false;
        this.CCNData=this.dashboardService.getCCNdata();
      }
    }else{
      
      if(event.target.originalArgument === 'Overdue' && this.selectedDepartments[0].itemName==='QA'){
        console.log('reached');
        this.overdueData=this.dashboardService.getOverdueData();
      }
    }
  }

  customizeLabel(arg) {
    return arg.argumentText + " (" + arg.valueText + ")";
}

  customizeTooltip(arg: any) {
    return {
        text: arg.seriesName + ' : ' + arg.valueText
    };
}

customizeTooltipForOverdue = (info: any) => {
  return {
      html: "<div><div class='tooltip-header'>" +
          info.argumentText + "</div>" +
          "<div class='tooltip-body'><div class='series-name'>" +
          "<span class='top-series-name'>" + info.points[0].seriesName + "</span>" +
          ": </div><div class='value-text'>" +
          "<span class='top-series-value'>" + info.points[0].valueText + "</span>" +
          "</div><div class='series-name'>" +
          "<span class='bottom-series-name'>" + info.points[1].seriesName + "</span>" +
          ": </div><div class='value-text'>" +
          "<span class='bottom-series-value'>" + info.points[1].valueText + "</span>" +
          "% </div></div></div>"
  };
}

customizeLabelText = (info: any) => {
  return info.valueText + "%";
}



}
