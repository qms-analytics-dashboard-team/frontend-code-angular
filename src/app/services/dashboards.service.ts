import { Injectable } from '@angular/core';

export interface QualityEventsDataStructure{
  department:string
  ccn:number
  incident:number
  errata:number
  noteToFile:number
}

export interface CCNDataStructure{
  CCNstatus:string
  CCNstatusValue:number
}

export interface OverdueDataStructure{
  timeFrame:string
  count:number
  // overdueStatus:string
  // // overdueStatusValue:number
  // lessThanOneWeek:number
  // oneWeekToOneMonth:number
  // oneMonthsToThreeMonths:number
  // threeToSixMonths:number
  // moreThanSixMonths:number
}


@Injectable({
  providedIn: 'root'
})


export class DashboardsService {

  qualityEventsData: QualityEventsDataStructure[] = [{
    department: "QA",
    ccn: 6,
    incident: 28,
    errata:5,
    noteToFile: 8
}, {
    department: "HR",
    ccn: 9,
    incident: 43,
    errata:12,
    noteToFile: 9
}, {
    department: "Ware house",
    ccn: 13,
    incident: 49,
    errata:12,
    noteToFile: 5
}];

CCNdata: CCNDataStructure[] = [{
  CCNstatus: "Rejected",
  CCNstatusValue: 11
}, {
  CCNstatus: "Approved",
  CCNstatusValue: 15
}, {
  CCNstatus: "Overdue",
  CCNstatusValue: 6
}, {
  CCNstatus: "Closed",
  CCNstatusValue: 18
}, {
  CCNstatus: "Pending",
  CCNstatusValue: 12
}];

// overdueData:OverdueDataStructure[]=
//   [
//     {
//       overdueStatus:'lessThanOneWeek',
//       overdueStatusValue:3
//     },
//     {
//       overdueStatus:'oneWeekToOneMonth',
//       overdueStatusValue:5
//     },
//     {
//       overdueStatus:'oneMonthsToThreeMonths',
//       overdueStatusValue:12
//     },
//     {
//       overdueStatus:'threeToSixMonths',
//       overdueStatusValue:10
//     },
//     {
//       overdueStatus:'moreThanSixMonths',
//       overdueStatusValue:8
//     }
//   ] 

overdueData:OverdueDataStructure[]=
[{ timeFrame: "< 1 Week", count: 13 },
{ timeFrame: "1 Week - 1 Month", count: 5 },
{ timeFrame: "1 - 3 Months", count: 2 },
{ timeFrame: "3 - 6 Months", count: 10 },
{ timeFrame: "> 6 Months", count: 8 },
]  


  constructor() { }
 
  getQualityEventsData(deptName): QualityEventsDataStructure[] {
    return deptName==='' ? this.qualityEventsData : this.qualityEventsData.filter(i=>i.department===deptName);
    // if(deptName===''){
    //   return this.qualityEventsData;
    // }else{
    //   return this.qualityEventsData.filter(i=>i.department===deptName);
    // }    
  }

  getQualityEventsDataOfSelectedDeptAndEvent(qualityEvents,deptName):QualityEventsDataStructure[]{
    // const obj = {id: 1, libelle: 'test', mustIgnore: 'test'};

    // // Destructure into mustIgnore and rest properties
    // const { mustIgnore, ...rest } = obj;
    
    // const projectedObject = rest;
     
    // console.log("old", obj);
    // console.log("new", projectedObject);

    let currentDepartmentData=this.getQualityEventsData(deptName);
      for(let i=0;i<qualityEvents.length;i++){
        let itemName= qualityEvents[i].itemName;
        if(itemName==='CCN'){
          delete currentDepartmentData[0].ccn;
        }else if(itemName==='Errata'){
          delete currentDepartmentData[0].errata;
        }
        else if(itemName==='Incident'){
          delete currentDepartmentData[0].incident;
        }else if(itemName==='Note to File'){
          delete currentDepartmentData[0].noteToFile;
        }
        // let eventName = qualityEvents[i].itemName
        // const {eventName, ...rest}=currentDepartmentData[i];
      }
      console.log("currentDepartmentData",currentDepartmentData);
      return;
    }
      

  getCCNdata(): CCNDataStructure[] {
  return this.CCNdata;
}

getOverdueData():OverdueDataStructure[]{
//   var data = this.overdueData.sort(function (a, b) {
//     return b.count - a.count;
// });
var localData = this.overdueData;
var totalCount = localData.reduce(function (prevValue, item) {
    return prevValue + item.count;
}, 0),
cumulativeCount = 0;
return localData.map(function (item, index) {
cumulativeCount += item.count;
return {
    timeFrame: item.timeFrame,
    count: item.count,
    cumulativePercent: Math.round(cumulativeCount * 100 / totalCount)
};
});
}
  // return this.overdueData;
}
